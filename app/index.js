const rp = require('request-promise');
const axios = require('axios');
const $ = require('cheerio');
const url = 'https://csgostash.com/skin';
const https = require('https');
const fs = require('fs');
const path = require('path');
const amount = 1268;

const getImage = (i) => {
    rp(`${url}/${i}`)
        .then((html) => {
            const fileUrl = $('img.main-skin-img', html).attr('src');
            if(fileUrl) {
                const gunName = $('h2 > a', html).first().html();
                const skinName = $('h2 > a', html).last().html();
                const file = fs.createWriteStream(path.join(__dirname, 'img', `${gunName}-${skinName}-${i}.jpg`));
                console.log("INDEX = ",i);
                return https.get(fileUrl, (response) => {
                    response.pipe(file);
                });
            }
            return null;
        })
        .catch((err) => {
            // console.log(err);
            return err;
        });
};

function delay(i) {
    return new Promise(resolve => setTimeout(() => resolve(getImage(i + 90)), 8000));
}

async function processArray(array) {
    array.forEach(async (item) => {
        await delay(item);
        // console.log("item proccessed");
    })
    console.log('Done!');
}


const images = Array.from(Array(10).keys())
processArray(images);
Promise.all(images);
//http://steamcommunity.com/market/listings/730/{UMP-45}%20%7C%20{Caramel}%20%28{Well-Worn}%29







